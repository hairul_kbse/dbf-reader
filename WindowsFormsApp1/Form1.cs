﻿using DbfDataReader;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Odbc;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            listView1.View = View.Details;
            listView1.GridLines = true;
            listView1.FullRowSelect = true;

            //Add column header
            listView1.Columns.Add("No.", 50);
            listView1.Columns.Add("Attribute", 200);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textBox2.Text = openFileDialog1.FileName;

                var dbfPath = openFileDialog1.FileName;
                using (var dbfTable = new DbfTable(dbfPath, EncodingProvider.GetEncoding(1252)))
                {
                    var header = dbfTable.Header;

                    var versionDescription = header.VersionDescription;
                    var hasMemo = dbfTable.Memo != null;
                    var recordCount = header.RecordCount;

                    ListViewItem listViewItem = new ListViewItem();
                    int i = 1;
                    string[] arr = new string[4];
                    foreach (var dbfColumn in dbfTable.Columns)
                    {

                        var name = dbfColumn.Name;
                        ListViewItem itm;
                        //add items to ListView
                        arr[0] = i.ToString();
                        arr[1] = name;
                        itm = new ListViewItem(arr);
                        listView1.Items.Add(itm);

                        var columnType = dbfColumn.ColumnType;
                        var length = dbfColumn.Length;
                        var decimalCount = dbfColumn.DecimalCount;
                        i++;
                    }
                }

            }
        }
    }
}
